import { Container, Nav, Navbar, Button } from "react-bootstrap";
import { NavLink } from "react-router-dom";
import { useState, useContext } from "react";
import UserContext from "../UserContext";

export default function AppNavbar() {
  const { user } = useContext(UserContext);
  console.log(user);
  return (
    <Navbar bg="light" expand="lg">
      <Container fluid>
        <Navbar.Brand as={NavLink} to="/" end>
          Joe's Ecommerce
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="ms-auto">
            {user.isAdmin ? (
              <>
                <Nav.Link as={NavLink} to="/admin" end>
                  Admin Dashboard
                </Nav.Link>
                <Nav.Link as={NavLink} to="/statistics" end>
                  Sales Analysis
                </Nav.Link>
              </>
            ) : (
              <>
                <Nav.Link as={NavLink} to="/" end>
                  Home
                </Nav.Link>
                <Nav.Link as={NavLink} to="/cart" end>
                  Cart
                </Nav.Link>
                <Nav.Link as={NavLink} to="/products" end>
                  Products
                </Nav.Link>
              </>
            )}
            {user.id !== null ? (
              <>
                <Nav.Link as={NavLink} to="/logout" end>
                  Logout
                </Nav.Link>
              </>
            ) : (
              <>
                <Nav.Link as={NavLink} to="/login" end>
                  Login
                </Nav.Link>
                <Nav.Link as={NavLink} to="/register" end>
                  Register
                </Nav.Link>
              </>
            )}
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}
