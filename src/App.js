import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import { Container } from "react-bootstrap";
import { useState, useEffect } from "react";

import AppNavbar from "./components/AppNavbar";
import Home from "./pages/Home";
import Register from "./pages/Register";
import Store from "./pages/Store";
import Login from "./pages/Login";
import Cart from "./pages/Cart";
import Logout from "./pages/Logout";
import NotFound from "./pages/NotFound";
import ProductView from "./pages/ProductView";
import Products from "./pages/Products";
import AdminDashboard from "./pages/AdminDashboard";

import "./App.css";
import { UserProvider } from "./UserContext";

function App() {
  const [user, setUser] = useState({
    id: null,
    isAdmin: null,
  });
  const unsetUser = () => {
    localStorage.clear();
  };

  useEffect(() => {
    console.log(user);
    console.log(localStorage);
  }, [user]);
  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details/`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        if (data._id !== undefined) {
          setUser({
            id: data._id,
            isAdmin: data.isAdmin,
          });
        } else {
          setUser({
            id: null,
            isAdmin: null,
          });
        }
      });
  }, []);
  return (
    <UserProvider value={{ user, setUser, unsetUser }}>
      <Router>
        <AppNavbar />
        <Container>
          <Routes>
            {user.id !== null ? (
              <>
                <Route exact path="/logout" element={<Logout />} />
                <Route exact path="/login" element={<Store />} />
                <Route exact path="/register" element={<Store />} />
              </>
            ) : (
              <>
                <Route exact path="/logout" element={<Store />} />
                <Route exact path="/login" element={<Login />} />
                <Route exact path="/register" element={<Register />} />
              </>
            )}
            <Route exact path="/" element={<Home />} />
            <Route exact path="/cart" element={<Cart />} />

            <Route exact path="/admin" element={<AdminDashboard />} />
            <Route exact path="/products" element={<Products />} />
            <Route
              exact
              path="/products/:productId"
              element={<ProductView />}
            />
            <Route exact path="*" element={<NotFound />} />
          </Routes>
        </Container>
      </Router>
    </UserProvider>
  );
}
export default App;
