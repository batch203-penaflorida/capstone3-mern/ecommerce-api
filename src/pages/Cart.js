import { useEffect, useState, useContext } from "react";
import { Button, Modal } from "react-bootstrap";
import { Navigate } from "react-router-dom";
import CartCard from "../components/CartCard";
import UserContext from "../UserContext";
import Swal from "sweetalert2";
// import coursesData from "../data/coursesData";

// course state tht will be used to store courses retrieve in the database.

export default function Cart() {
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const [showEdit, setShowEdit] = useState(false);
  const { user } = useContext(UserContext);
  const [carts, setCarts] = useState([]);
  const [totalAmount, setTotalAmount] = useState([]);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/orders`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
      });
  });
  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/carts/`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data.products);
        const subTotal = data.products.map((cart) => {
          return cart.subTotal;
        });
        console.log(subTotal);
        let sum = subTotal.reduce(function (a, b) {
          return a + b;
        }, 0);
        sum.toFixed(2);
        setTotalAmount(sum);
        setCarts(
          data.products.map((cart) => {
            return <CartCard key={cart.productId} cartProp={cart} />;
          })
        );
      });
  }, [carts]);

  const checkOut = () => {
    fetch(`${process.env.REACT_APP_API_URL}/carts/checkOut`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if (data) {
          Swal.fire({
            title: "Place Order Successful",
            icon: "success",
            text: `Order was placed successfully`,
          });
        } else {
          Swal.fire({
            title: "Failed To Place Order",
            icon: "error",
            text: `Please try again later.`,
          });
        }
      });
  };
  return user.isAdmin ? (
    <Navigate to="/admin" />
  ) : (
    <>
      <Button variant="success" className="my-2" onClick={handleShow}>
        Show Orders
      </Button>
      <h1 className="my-3">Cart</h1>
      {carts}
      <p>Total Amount: ₱ {totalAmount}</p>

      <Button
        size="sm"
        onClick={() => {
          checkOut();
        }}
      >
        Place Order
      </Button>

      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Order History</Modal.Title>
        </Modal.Header>
        <Modal.Body></Modal.Body>
      </Modal>
    </>
  );
}
