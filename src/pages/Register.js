import { useEffect, useState } from "react";

import { useNavigate } from "react-router-dom";
import Swal from "sweetalert2";
import { Button, Form, Card } from "react-bootstrap";

export default function Register() {
  // Create state hooks to store the values of the input fields.
  const navigate = useNavigate();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [cpassword, setCpassword] = useState("");
  const [fName, setfName] = useState("");
  const [mobileNo, setMobileNo] = useState("");
  const [street, setStreet] = useState("");
  const [city, setCity] = useState("");
  const [region, setRegion] = useState("");
  const [country, setCountry] = useState("");
  const [postalCode, setPostalCode] = useState("");

  const [isActive, setIsActive] = useState(false);
  useEffect(() => {
    if (
      email !== "" &&
      password !== "" &&
      cpassword !== "" &&
      fName !== "" &&
      mobileNo !== "" &&
      street !== "" &&
      city !== "" &&
      region !== "" &&
      country !== "" &&
      postalCode !== "" &&
      password === cpassword
    ) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [
    email,
    password,
    cpassword,
    fName,
    mobileNo,
    city,
    street,
    region,
    country,
    postalCode,
  ]);

  function registerUser(e) {
    e.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        email: email,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if (data) {
          Swal.fire({
            title: "Duplicate email found",
            icon: "error",
            text: "Kindly provide another email to complete the registration.",
          });
        } else {
          fetch(`${process.env.REACT_APP_API_URL}/users/checkMobile`, {
            method: "POST",
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify({
              mobileNo: mobileNo,
            }),
          })
            .then((res) => res.json())
            .then((data) => {
              if (data) {
                Swal.fire({
                  title: "Duplicate Mobile Number found",
                  icon: "error",
                  text: "Kindly provide another mobile number to complete the registration",
                });
              } else {
                fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
                  method: "POST",
                  headers: {
                    "Content-Type": "application/json",
                  },
                  body: JSON.stringify({
                    email: email,
                    password: password,
                    mobileNo: mobileNo,
                    fullName: fName,
                    street: street,
                    city: city,
                    region: region,
                    country: country,
                    postalCode: postalCode,
                  }),
                })
                  .then((res) => res.json())
                  .then((data) => {
                    console.log(data);
                    if (data) {
                      Swal.fire({
                        title: "Registration Successful",
                        icon: "success",
                        text: "Welcome to Zuitt!",
                      });
                      setEmail("");
                      setPassword("");
                      setCpassword("");
                      // Allow us to redirect the user to the login page after account registration

                      navigate("/login");
                    } else {
                      Swal.fire({
                        title: "Something went wrong",
                        icon: "error",
                        text: "Please try again.",
                      });
                    }
                  });
              }
            });
        }
      });
    // Notify user for registration
  }
  return (
    <>
      <Card className="my-5">
        <Card.Body>
          <h1>Register</h1>
          <Form onSubmit={(e) => registerUser(e)}>
            <Form.Group className="mb-3" controlId="email">
              <Form.Label>Email address</Form.Label>
              <Form.Control
                type="email"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
                placeholder="Enter email"
                required
              />
              <Form.Text className="text-muted">
                We'll never share your email with anyone else.
              </Form.Text>
            </Form.Group>

            <Form.Group className="mb-3" controlId="password">
              <Form.Label>Password</Form.Label>
              <Form.Control
                type="password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
                placeholder="Enter password"
                required
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="cpassword">
              <Form.Label>Confirm Password</Form.Label>
              <Form.Control
                type="password"
                value={cpassword}
                onChange={(e) => setCpassword(e.target.value)}
                placeholder="Enter Confirm Password"
                required
              />
            </Form.Group>
            <hr />
            <Card.Title>Shipping Details</Card.Title>
            <Form.Group className="mb-3" controlId="fName">
              <Form.Label>Full Name</Form.Label>
              <Form.Control
                type="text"
                value={fName}
                onChange={(e) => setfName(e.target.value)}
                placeholder="Enter Full Name"
                required
              />
            </Form.Group>

            <Form.Group className="mb-3" controlId="mobileNo">
              <Form.Label>Mobile Number</Form.Label>
              <Form.Control
                maxLength="11"
                type="text"
                value={mobileNo}
                onChange={(e) => setMobileNo(e.target.value)}
                placeholder="Enter Full Name"
                required
              />
            </Form.Group>

            <Form.Group className="mb-3" controlId="street">
              <Form.Label>Street</Form.Label>
              <Form.Control
                type="text"
                value={street}
                onChange={(e) => setStreet(e.target.value)}
                placeholder="Enter Street"
                required
              />
            </Form.Group>

            <Form.Group className="mb-3" controlId="city">
              <Form.Label>City</Form.Label>
              <Form.Control
                type="text"
                value={city}
                onChange={(e) => setCity(e.target.value)}
                placeholder="Enter City"
                required
              />
            </Form.Group>

            <Form.Group className="mb-3" controlId="region">
              <Form.Label>Region</Form.Label>
              <Form.Control
                type="text"
                value={region}
                onChange={(e) => setRegion(e.target.value)}
                placeholder="Enter Region"
                required
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="country">
              <Form.Label>Country</Form.Label>
              <Form.Control
                type="text"
                value={country}
                onChange={(e) => setCountry(e.target.value)}
                placeholder="Enter Country"
                required
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="postalCode">
              <Form.Label>Postal Code</Form.Label>
              <Form.Control
                maxLength={4}
                type="text"
                value={postalCode}
                onChange={(e) => setPostalCode(e.target.value)}
                placeholder="Enter Postal Code"
                required
              />
            </Form.Group>

            {isActive ? (
              <Button
                variant="primary"
                className="my-3"
                type="submit"
                id="submitBtn"
              >
                Submit
              </Button>
            ) : (
              <Button
                variant="danger"
                className="my-3"
                type="submit"
                id="submitBtn"
                disabled
              >
                Submit
              </Button>
            )}
          </Form>
        </Card.Body>
      </Card>
    </>
  );
}
